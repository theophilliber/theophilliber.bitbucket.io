# -*- coding: utf-8 -*-
"""!@file taskBNO055.py
@brief IMU class used to create IMU objects to read and write Accelerometer and
        Gyroscope data.
@details The class has the attributes... and methods ... 
"""
from pyb import I2C
import BNO055, micropython
from time import ticks_us, ticks_add, ticks_diff
S0_INIT = micropython.const(0)
S1_CALIB = micropython.const(1)
S2_STAY = micropython.const(2)




def taskIMUFcn(taskName, period, keyFlag, taskFin, valueShare, velShare, posShare, timeShare, CLC, rollShare, pitchShare):
    '''!@brief              FSM code that runs user interface.
        @details            The task reads user key inputs and raises the corresponding flag.
                            Flags rasied are read read by motor or IMU to perform desired function.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param keyFlag      Flag containing value of user inputted key command
        @param taskFin      Share telling if task has finished across task files,
                            similar to acknowledge bit in I2C communication
        @param valueShare   Share holding value to transport across tasks. (Duty
                            cycles, gain values, etc.)
        @param velShare     Share holding current value of velocity.
        @param posShare     Share holding current value of encoder position.
        @param timeShare    Share holding current value of time. Could be calculated
                            in each task, but more accurate to find at time of pos/vel data.
        @param CLC          Share telling whether closed-loop control is on or off. By
                            default, the motor is in open-loop control (CLC = False)
        @param rollShare    Share that stores roll angle of the platform from IMU readings
        @param pitchShare   Share that stores pitch angle of the platform from IMU readings
        
                            
                            
    '''
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    state = 0
    while True:
        current_time = ticks_us()
            # Check if time period has passed since last time code ran.
        if ticks_diff(current_time, next_time) >= 0:
            # Update next_time value for next code iteration.
            next_time = ticks_add(next_time, period)
    # Create instance of BNO055 IMU Driver class and I2C communication protocol
    
    #State 0 INIT
            if state == S0_INIT:
                myI2C = I2C(1, I2C.CONTROLLER)
                myIMU = BNO055.BNO055(myI2C)
                myIMU.IMU_mode()
                print('The IMU is not fully calibrated yet. Move it around until it finishes calibrating.')
                state = S1_CALIB
                
            # Check if IMU Calibration Coefficient file (IMU_cal_coeffs.txt) exists in pybflash
            
    # =============================================================================
    #         try:
    #             file = open('IMU_cal_coeffs.txt', 'r')
    #             cal_coeffs = file.read()
    #             file.close()
    #             
    #         except:
    #             print('file not read')
    #             print('User needs to calibrate the IMU. Move the platform around until interface says it is done calibrating.')
    #             state = 1
    # =============================================================================
                
            elif state == S1_CALIB:
                myIMU.calib_status()
                if (myIMU.acc_stat==3) and (myIMU.gyr_stat==3):
                    taskFin.write(True)
                    print('Calibration done!')
                    cal_coeffs = str(myIMU.calib_read())
                    with open('IMU_cal_coeffs.txt', 'w') as f:
                        #print('Writing IMU file')
                       # calString = f.write(','.join(hex(cal_coeff) for cal_coeff in cal_coeffs))
                        #str_list = []
                        #for cal_coeff in cal_coeffs:
                            #str_list.append(cal_coeff)
                        f.write(cal_coeffs)
                        state = S2_STAY
                            #f.close()
                        #print(cal_coeffs)
            
            #State 2: read/update encoders to pass positional data to motortask
            elif state == S2_STAY:
                myIMU.euler_angle()
                rollShare.write(myIMU.roll)
                pitchShare.write(myIMU.pitch)
                
                pass
                
                
                
            
            yield state
            
        else:
            yield state