'''!@file DRV8847.py
@brief A motor class for DC motor with encoder
'''
from motor4 import Motor
from pyb import Timer, Pin
import pyb, time

class DRV8847:
    '''!@brief A motor driver class for the DRV8847 from TI.
    @details Objects of this class can be used to configure the DRV8847
    motor driver and to create one or moreobjects of the
    Motor class which can be used to perform motor
    control.
    Refer to the DRV8847 datasheet here:
    https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    
    def __init__ (self):
        '''!@brief Initializes and returns a DRV8847 object.
        '''
        self.nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP)
        self.nFAULT = Pin(Pin.cpu.B2, mode=pyb.ExtInt.IRQ_FALLING)
        pass
        
    def enable (self):
        '''!@brief Brings the DRV8847 out of sleep mode.
        '''
        self.nSLEEP.high()
        pass
        
    def disable (self):
        '''!@brief Puts the DRV8847 in sleep mode.
        '''
        print('disabling')
        self.nSLEEP.low()        
        pass
        
    def fault_cb (self, IRQ_src):
        '''!@brief Callback function to run on fault condition.
        @param IRQ_src The source of the interrupt request.
        '''
        #self.disable()
        #self.FaultFlag = True
        
        self.FaultInt = pyb.ExtInt(self.nFAULT, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_UP, callback=self.disable)
        #self.FaultInt = pyb.ExtInt(pyb.Pin.cpu.C13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_UP, callback=self.disable())
        
        
        pass
    
    def Motorf (self, PWM_tim, timch1, timch2, IN1_pin, IN2_pin):
        '''!@brief Creates a DC motor object connected to the DRV8847.
        @return An object of class Motor
        '''
        self.Motor = Motor(PWM_tim, timch1, timch2, IN1_pin, IN2_pin)
        
        
        
        #motor_1 = Motor(PWM_tim, 1, 2, Pin.cpu.B4, Pin.cpu.B5)
        #motor_2 = Motor(PWM_tim, 3, 4, Pin.cpu.B0, Pin.cpu.B1)
        return self.Motor
        
if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    
    # Create a motor driver object and two motor objects. You will need to
    # modify the code to facilitate passing in the pins and timer objects needed
    # to run the motors.
   
    
    motor_drv = DRV8847()
    PWM_tim = Timer(3, freq = 20_000)
        
    motor_1 = motor_drv.Motorf(PWM_tim, 1, 2, Pin.cpu.B4, Pin.cpu.B5)
    motor_2 = motor_drv.Motorf(PWM_tim, 3, 4, Pin.cpu.B0, Pin.cpu.B1)
    # Enable the motor driver
    motor_drv.enable()
    #motor_drv.disable()
    # Set the duty cycle of the first motor to 40 percent and the duty cycle of
    # the second motor to 60 percent
    motor_1.set_duty(60)
    motor_2.set_duty(30)
    
    #time.sleep(5)
    #motor_drv.disable()
    
    #execfile('DRV8847.py')
   
    
    #while True:
        #try:
            #pass
            #pyb.ExtInt(pyb.Pin.cpu.C13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_UP, callback=motor_drv.fault_cb())
        #except KeyboardInterrupt:
            #motor_drv.fault_cb(pyb.Pin.cpu.C13)
            #break
    #motor_drv.disable()
    #print("Program Terminating")
    
    
    