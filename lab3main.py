"""!@file lab3main.py
@brief Main file for lab 3. Added functionality of motor actuation.
@page lab3 Lab 0x03: PMDC Motors
@brief      Added functionality of motor actuation.
@n
@details    This code reads input from a quadrature incremental encoder. There is a 
            user interface (taskUser.py) which allows the user to control the 
            encoder and motor to perform several functions. It also prints out a description
            of what the functions are and how to execute them. The file taskEncoder.py
            acts as an interface between the UI file and the encoder driver (encoder.py),
            calling methods from the encoder and passing them to the UI to print.
            Similarly, taskMotor.py acts as an interface between
            the UI file and the motor driver (DRV8847.py).The driver file sets up an encoder 
            class and creates the methods used by the other files.
            
    
@image html TransitionDiagram3.png
@n
@details    This is the task diagram used to design the FSM states of each component.
@image html 'Taskdiagram3.png'
@n
@image html Dutycyclevsspeed.png
@n
@details    The output presented is the average angular velocity of motor 1 at each set dtuy cycle.
@n
Source Folder
    [https://ruodolfr.bitbucket.io/index.html]
@n
Demo Video
    [Link]

@author Theo Philliber
@author Ruodolf Rumbaoa
@date 02/02/2022
"""
import taskUser3, taskEncoder3, shares, taskMotor3

if __name__ == '__main__':
    
    # Shares for button presses
    zFlag = shares.Share(False)
    pFlag = shares.Share(False)
    dFlag = shares.Share(False)
    gFlag = shares.Share(False)
    sFlag = shares.Share(False)
    vFlag = shares.Share(False)
    mFlag = shares.Share(False)
    MFlag = shares.Share(False)
    cFlag = shares.Share(False)
    tFlag = shares.Share(False)
    duty1 = shares.Share(0)
    duty2 = shares.Share(0)
    
    #recDone -- flag indicating recording is finished
    recDone = shares.Share(False)
    #dataList - list variable to print out recorded data to PuTTY
    dataList = shares.Queue()
    timeList = shares.Queue()
    velList = shares.Queue()
    #printShare - variable to print position and delta values
    printShare = shares.Share(0)
    
    
    
    taskList = [taskUser3.taskUserFcn('Task User', 1_000, zFlag, pFlag, dFlag, gFlag, sFlag, vFlag, mFlag, MFlag, cFlag, tFlag, dataList, timeList, velList, printShare, recDone, duty1, duty2), 
                taskEncoder3.taskEncFcn('Task Encoder', 10_000, zFlag, pFlag, dFlag, gFlag, sFlag, vFlag, mFlag, MFlag, cFlag, tFlag, dataList, timeList, velList, printShare, recDone),
                taskMotor3.taskMotorFnc('Task Motor', 10_000, vFlag, mFlag, MFlag, cFlag, gFlag, tFlag, sFlag, duty1, duty2)]

    
    while True:
        
        try:
            for task in taskList:
                
              
                next(task)
           
        
        except KeyboardInterrupt:
        
            break
    print('Program Terminating')

