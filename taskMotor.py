'''!@file      taskMotor.py
    @brief     Task responsible for driving motor. 
    @details   Runs closed loop driver and motor driver. Reads data from touch
    panel and IMU, and runs through controllers to calculate duty cycles for motors.
    Outer loop control uses ball position from touchpad to create theta_ref values.
    These are passed into inner loop controllers, which output the motor duty cycles.
    If no ball is detected, theta_ref is zero, and the platform will balance upright.
    
    @author     Theo Philliber
    @author     Ruodolf Rumbaoa
    @date       03/18/2022
'''
from time import ticks_us, ticks_add, ticks_diff
import array
from pyb import Timer, Pin
import micropython

from motor import Motor
from closedLoop import ClosedLoop

##  @brief S0_INIT
#   @details Initializes taskMotorFcn function. Creates x and y motor objects, and
#   four controller objects -- Inner and outer loop control for x and y motors.
S0_INIT = micropython.const(0)
##  @brief S1_STAY
#   @details Keeps platform and ball upright. Reads IMU and touchpad data, and
#   passes into controller classes. Uses output to set duty cycles to drive motors.
S1_STAY = micropython.const(1)


def taskMotorFcn(taskName, period, ENABLE, KEYFLAG, TASKFIN, KPSHARE, KDSHARE, BALLPOS, BALLVEL, rollShare, pitchShare, rollvShare, pitchvShare):
     '''!@brief Motor task function
        @details Generator function that runs through states. Instances motor drivers
        (x and y), and creates controllers (x/y - inner/outer). Outer loop control
        uses ball position from touchpad to create theta_ref values. These are 
        passed into inner loop controllers, which output the motor duty cycles.
        If no ball is detected, theta_ref is zero, and the platform will balance upright.
        @param taskName Name of task
        @param period Minimum period of time between subsequent runs of function.
        @param ENABLE Motor control is enabled/disabled.
        @param KEYFLAG Share containing value of key pressed by user. 
        @param TASKFIN Boolean share acknowledging if an action has been completed.
        Used to verify synchronicity between tasks.
        @param KPSHARE Share containing Kp value, proportional gain.
        @param KDSHARE Share containing Kd value, derivative gain.
        @param BALLPOS Share containing tuple of ball position (x, y, contact).
        x and y are in mm; contact == 1 if ball is present, 0 if not.
        @param BALLVEL Share containing tuple of ball velocity (xdot, ydot).
        @param rollShare Contains value of roll angle from IMU.
        @param pitchShare Contains value of pitch angle from IMU.
        @param rollvShare Contains value of roll angular velocity from IMU.
        @param pitchvShare Contains value of pitch angular velocity from IMU.    
     '''
    
     state = S0_INIT
     
     PWM_tim = Timer(3, freq = 20_000)
    
    # Y MOTOR -- 
     ymotor = Motor(PWM_tim, 4, 3, Pin.cpu.B0, Pin.cpu.B1)
     
     # X MOTOR -- rotates 
     xmotor = Motor(PWM_tim, 1, 2, Pin.cpu.B4, Pin.cpu.B5)
     
     
     
     xDuty = 0
     yDuty = 0
     
     #X Inner Loop - X = [theta(y), theta_dot(y)]  Platform Balancing
     XInner = ClosedLoop(0,0, 370,-5)
     
     #X Outer Loop - X = [x, x_dot]  Ball positioning
     XOuter = ClosedLoop(0,0,0.003,0)

     #Y Inner Loop - X = [theta(x), theta_dot(x)]  Platform Balancing
     YInner = ClosedLoop(0,0,370,-5)
     
     #Y Outer Loop - X = [y, y_dot]  Ball positioning
     YOuter = ClosedLoop(0,0,0.003,0)






     #.enable(motor_1)
     start_time = ticks_us()
     next_time = ticks_add(start_time, period)
    
     state = S1_STAY
     while True:
        # Find current time
        current_time = ticks_us()
        # Check if time period has passed since last time code ran.
        if ticks_diff(current_time, next_time) >= 0:
            # Update next_time value for next code iteration.
            next_time = ticks_add(next_time, period)
    
    

                
            if state == S1_STAY:
                    
                if ENABLE.read() == True:
                    (x, y, contact) = BALLPOS.read()[0:3]
                    (x_dot, y_dot) = BALLVEL.read()
                    
                    if contact == 1:
                        theta_yref = XOuter.update(0, x, 0, x_dot)
                        if theta_yref > .15:
                            theta_yref = .15
                        elif theta_yref < -.15:
                            theta_yref = -.15
                            
                        theta_xref = YOuter.update(0, y, 0, y_dot)
                        if theta_xref > .12:
                            theta_xref = .12
                        elif theta_xref < -.12:
                            theta_xref = -.12
                    else:
                        theta_yref = 0
                        theta_xref = 0
                        x_dot = 0
                        y_dot = 0
                        
                        
                    xDuty = XInner.update(theta_yref, -pitchShare.read(), 0, pitchvShare.read())
                    yDuty = YInner.update(theta_xref, rollShare.read(), 0, rollvShare.read())
                    #print(rollShare.read(), pitchShare.read() )
                    if xDuty > 30:
                        xDuty = 30
                    elif xDuty < -30:
                        xDuty = -30
                    
                    if yDuty > 30:
                        yDuty = 30
                    elif yDuty < -30:
                        yDuty = -30
                    
                    xmotor.set_duty(xDuty)
                    ymotor.set_duty(yDuty)
                    #print(rollDuty, pitchDuty)
                else:    
                    xmotor.set_duty(0)
                    ymotor.set_duty(0)
                        
                        
                   
    
                if KEYFLAG.read() != None:
                #Set gain Kp for PID controller
                    if KEYFLAG.read() == 'p':
                        if KPSHARE.read() != None:
                            XInner.set_gain(KPSHARE.read(), 1)
                            YInner.set_gain(KPSHARE.read(), 1)
                            KPSHARE.write(None)
                            KEYFLAG.write(None)
                            TASKFIN.write(True)                  
                            
                            
                    elif KEYFLAG.read() == 'd':
                        if KDSHARE.read() != None:
                            XInner.set_gain(KDSHARE.read(), 2)
                            YInner.set_gain(KDSHARE.read(), 2)
                            KDSHARE.write(None)
                            KEYFLAG.write(None)
                            TASKFIN.write(True)
                    elif KEYFLAG.read() == 'n':
                        if KPSHARE.read() != None:
                            XOuter.set_gain(KPSHARE.read(), 1)
                            YOuter.set_gain(KPSHARE.read(), 1)
                            KPSHARE.write(None)
                            KEYFLAG.write(None)
                            TASKFIN.write(True) 
                    elif KEYFLAG.read() == 'm':
                        if KDSHARE.read() != None:
                            XOuter.set_gain(KDSHARE.read(), 2)
                            YOuter.set_gain(KDSHARE.read(), 2)
                            KDSHARE.write(None)
                            KEYFLAG.write(None)
                            TASKFIN.write(True)
                else:
                    pass

                
           
                
    
    
            
            
            yield state
        else:
            yield None