'''!@file      TaskMotor4.py
    @brief     Task responsible for motor driving. 
    @details   
    @author     Theo Philliber
    @author     Ruodolf Rumbaoa
    @date       02/17/2022
'''
from time import ticks_us, ticks_add, ticks_diff
import main4, array
from pyb import Timer, Pin
import encoder4, shares, micropython
from DRV8847 import DRV8847
import motor4
from closedLoop4 import ClosedLoop

S0_INIT = micropython.const(0)
S1_WAIT = micropython.const(1)
S2_DTY = micropython.const(2)
S3_CLR = micropython.const(3)
S4_TEST = micropython.const(4)
S5_CSV = micropython.const(5)
S17_STEP_TEST = micropython.const(17)





 
def taskMotorFnc(taskName, period, keyFlag, taskFin, valueShare, velShare, posShare, timeShare, CLC):
    '''!@brief         FSM code that runs the motor task.
        @details         The motor task reads flags raised by the user task and encoder task 
                       and performs the corresponding function.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                       number of microseconds.
                       
        @param keyFlag      Boolean variable (shares class) representing user key input
        @param taskFin      Share used as a flag raised when moving between states
        @param velShare     Share used for stroing motor velocity data
        @param posShare     Shares class used for storing motor shaft position data
                            of collected data. 
        @param valueShare   Stores duty cycle value to apply duty to motor 1 or motor 1
        @param timeShare    Stores time data used for calculating velocity
        @param CLC          Boolean variable that allows user to enable/disable closed
                            loop motor control
    '''    
    state = S0_INIT
    PWM_tim = Timer(3, freq = 20_000)
     
    motor_drv = DRV8847()
     
    motor_1 = motor_drv.Motorf(PWM_tim, 1, 2, Pin.cpu.B4, Pin.cpu.B5)
    motor_2 = motor_drv.Motorf(PWM_tim, 3, 4, Pin.cpu.B0, Pin.cpu.B1)
    motor_drv.enable()
     
    duty1 = 0
    duty2 = 0
    motorDuty1 = duty1
    motorDuty2 = duty2
     
    PID = ClosedLoop(0, 0)
    setpoint = 0
     #.enable(motor_1)
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    state = S1_WAIT
    while True:
        # Find current time
        current_time = ticks_us()
        # Check if time period has passed since last time code ran.
        if ticks_diff(current_time, next_time) >= 0:
            # Update next_time value for next code iteration.
            next_time = ticks_add(next_time, period)
    
                
            if state == S1_WAIT:
                #myEncoder1.update()
                
                if CLC.read():
                    motorDuty1 = PID.update(setpoint, velShare.read())
                    motor_1.set_duty(motorDuty1)
                    pass
                    
                elif duty1 != motorDuty1:
                    motor_1.set_duty(duty1)
                    state = S1_WAIT
                    
                if keyFlag.read() == 'm':
                    #print('Getting delta of encoder reading...')
                    #printShare.write(myEncoder1.get_delta())
                    #print('Enter a duty cycle for motor 2')
                    
                    if valueShare.read() != None:
                        duty1 = valueShare.read()
                        valueShare.write(None)
                        motor_1.set_duty(duty1)
                        keyFlag.write(None)
                        taskFin.write(True)
                        state = S1_WAIT
                        
                elif keyFlag.read() == 'M':
                    #print('Getting delta of encoder reading...')
                    #printShare.write(myEncoder1.get_delta())
                    #print('Enter a duty cycle for motor 2')
                    if valueShare.read() != None:
                        duty2 = valueShare.read()
                        valueShare.write(None)
                        motor_2.set_duty(duty2)
                        keyFlag.write(None)
                        taskFin.write(True)
                        state = S1_WAIT
                elif keyFlag.read() == 'r':
                    
                    if valueShare.read():
                        taskFin.write(True)
                        gain, setpoint = valueShare.read()
                        valueShare.write(None)
                        keyFlag.write(None)
                        PID.set_gain(gain, 1)
                        testStart = current_time
                        dutyList = array.array('f', 305*[0])
                        i = 0
                        state = S17_STEP_TEST
           
    # =============================================================================
    #             
    #                 elif cFlag.read():
    #                     #print('Recording data for 30 seconds. Press the s key to stop recording early. Output will be in a .CSV file')
    #                     #record_start_time = ticks_us()
    #                     pass
    #                     #timeArray = array.array('l', 3001*[0])
    #                     #posArray = array.array('l', 3001*[0])
    #                     #i = 0
    #                     #state = S5_RECORD
    #                 elif gFlag.read():
    #                     #print('Getting delta of encoder reading...')
    #                     #printShare.write(myEncoder1.get_delta())
    #                     gFlag.write(False)
    #                     state = S1_WAIT
    #                     
    #                 elif tFlag.read():
    #                     #print('Getting delta of encoder reading...')
    #                     #printShare.write(myEncoder1.get_delta())
    #                     tFlag.write(False)
    #                     state = S1_WAIT
    # =============================================================================
                
                #Set gain Kp for PID controller
                elif keyFlag.read() == 'k':
                    if valueShare.read() != None:
                        PID.set_gain(valueShare.read(), 1)
                        valueShare.write(None)
                        keyFlag.write(None)
                        taskFin.write(True)
                        state = S1_WAIT
               
                #Set setpoint, reference speed for PID
                elif keyFlag.read() == 'y':
                    if valueShare.read() != None:
                        setpoint = valueShare.read()
                        valueShare.write(None)
                        keyFlag.write(None)
                        taskFin.write(True)
                        state = S1_WAIT
                    
                
            elif state == S17_STEP_TEST:
                
                if ticks_diff(current_time, testStart + 1_000_000) <= 0:
                    motor_1.set_duty(0)
                    dutyList[i] = 0
                    i += 1
                elif ticks_diff(current_time, testStart + 3_000_000) <= 0:
                    motorDuty1 = PID.update(setpoint, velShare.read())
                    motor_1.set_duty(motorDuty1)
                    dutyList[i] = motorDuty1
                    i += 1
                else:
                    motor_1.set_duty(0)
                    PID.set_gain(0, 1)
                    valueShare.write(dutyList)
                    taskFin.write(True)    
                    state = S1_WAIT
                        
                
                
                        
                    
                
                    
                    
            #elif state == S5_RECORD:
                
             #   if i <= len(timeArray):
              #      myEncoder1.update()
                   
               #     if i % 1000 == 0:
                #        timeArray[i], posArray[i] = (current_time - record_start_time, myEncoder1.get_position())
                #    i += 1
                    
                #if ticks_diff(ticks_us(), record_start_time) >= 3000000:
               # if sFlag.read():
                #    dataList.write(posArray)
                 #   recDone.write(True)
                  #  sFlag.write(False)
                   # gFlag.write(False)
                    #state == S1_WAIT
                #elif current_time > record_start_time + 30_000_000:
                 #   dataList.write(posArray)
                  #  recDone.write(True)
                   # sFlag.write(False)
                    #gFlag.write(False)
                    #state == S1_WAIT
                    
               # else:                    
                    #myEncoder1.update()
                    #data_Array[i] = myEncoder1.get_position();
                    #if i % 10 == 0: 
                     #   data_Array.append(myEncoder1.get_position())
                    #i += 1
                                
    
            
            
            yield state
        else:
            yield None