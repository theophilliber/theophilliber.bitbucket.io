'''!@file      taskUser.py
    @brief      User Interface FSM code. 
    @details    The task uses the USB VCP (Virtual COM Port) to cooperatively 
                take character input from the user working at a serial terminal
                such as PuTTY. Using the keyboard, the user can perform the following
                functions: enable/disable motor control (e), begin touch panel calibration (c), 
                input proportional gain (p), input derivative gain (d). 
    @n
    @author     Theo Philliber
    @author     Ruodolf Rumbaoa
    @date       03/10/2022
'''

from time import ticks_us, ticks_add, ticks_diff
import micropython, pyb 



S0_INIT = micropython.const(0)
S1_WAIT = micropython.const(1)
S2_CALTP = micropython.const(2)
S3_NUMIN = micropython.const(3)
S4_GAIN = micropython.const(4)


def taskUserFcn(taskName, period, ENABLE, KEYFLAG, TASKFIN, KPSHARE, KDSHARE):
 


    '''!@brief              Generator function to run the UI task as an FSM.
        @details            The task runs as a generator function and requires a
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param ENABLE       Share used to communicate when motor control is enabled/disabled
        @param KEYFLAG      Flag containing value of user inputted key command
        @param TASKFIN      Share telling if task has finished across task files,
                            similar to acknowledge bit in I2C communication
        @param KPSHARE      Share holding value of proportional gain for inner control loop
        @param KDSHARE      Share holding value of derivative gain for inner control loop
    '''
    
    # Start at State 0, initialize welcome message with instructions
    state = S0_INIT
    numBuf = "" 
    print('+---------------------------------------------------+')
    print('+               ME305 TERM PROJECT                  +')
    print('+              BY THEO PHILLIBER AND                +')
    print('+                RUODOLF RUMBAOA                    +')
    print('+---------------------------------------------------+')
    print('+   e:  Enable/disable motor control                +')
    print('+   c:  Calibrate touch panel                       +')
    print('+   p:  Set proportional gain for controller        +')
    print('+   d:  Set derivative gain for controller          +')
    print('+   n:  Set proportional gain for outer controller  +')
    print('+   m:  Set derivative gain for outer controller    +')
    print('+---------------------------------------------------+')
    print("")
    print('The IMU is not fully calibrated yet. Move it around') 
    print('until it finishes calibrating. Motors will actuate ')
    print('once they are enabled by pressing (e or E), and    ')
    print('once controller gains are inputted.                ')
    print("")
    print('Initial Inner Loop Gains Kp = 370, Kd = -5')
    print('Initial Outer Loop Gains Kp = 0.003, Kd = 0.0001')   
    print("")
    # Create virtual serial port object for getting keyboard input.
    ser = pyb.USB_VCP()
    
    # Transition to State 1: Wait for input. 
    state = S1_WAIT
    
    # Enter while loop to continually check for keyboard input. Upon input from
    # user, the state will change and the corresponding flag will raise. Upon
    # the next iteration of the taskIMU.py or the taskMotor.py tasks, the flag will be read,
    # and the correspondingf method from BNO055.py or motor.py will be called.
    
   
   
    # Establish start time to control rate at which code runs.
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        # Find current time
        current_time = ticks_us()
        # Check if time period has passed since last time code ran.
        if ticks_diff(current_time, next_time) >= 0:
            # Update next_time value for next code iteration.
            next_time = ticks_add(next_time, period)
            
            # State 1: Wait for input. 
            # Check for input, read it (if existing), and raise the correct flag.
            if state == S1_WAIT:
                
                # Check for keyboard input.
                if ser.any():
                    
                    # Read (1 byte) of keyboard input. Raise whatever flag 
                    # corresponds to it.
                    charin = ser.read(1).decode() 
                    #print(charin)
                    
                    #Read character, change value of keyFlag and change state.
                    if charin in {'e', 'E'}:
                        if ENABLE.read() == False:
                            print('Enabling motor control')
                            ENABLE.write(True)
                        else:
                            print('Disabling motor control')
                            ENABLE.write(False)
                        state = S1_WAIT
                        
                    elif charin in {'c', 'C'}:
                        print('Beginning touch panel calibration procedure')
                        KEYFLAG.write('c')
                        TASKFIN.write(None)
                        state = S2_CALTP
                    elif charin in {'p', 'P'}:
                        print('Enter a proportional gain value for controller')
                        KEYFLAG.write('p')
                        state = S3_NUMIN
                    elif charin in {'d', 'D'}:
                        print('Enter a derivative gain value for controller')
                        KEYFLAG.write('d')
                        state = S3_NUMIN
                    elif charin in {'n', 'N'}:
                        print('Enter a proportional gain value for touchpad controller')                        
                        KEYFLAG.write('n')
                        state = S3_NUMIN
                    elif charin in {'m', 'M'}:
                        print('Enter a derivative gain value for touchpad controller')                        
                        KEYFLAG.write('m')
                        state = S3_NUMIN
            #State 2: Wait for touchpad clibration sequence to finish
            #Once finished, TASKFIN will be raised by IMU task and completion message 
            #is printed by user task
            elif state == S2_CALTP:
                #Maybe add 's' key functionality to stop calibration sequence
                if TASKFIN.read():
                    print("Calibration Complete! Good job")
                    TASKFIN.write(None)
                    state = S1_WAIT
                    
                
            #State 3: Read number inputs from user. Makes a buffer of number inputs then
            #moves to state 4
            elif state == S3_NUMIN:
                
                if ser.any():
                    char = ser.read(1).decode()
                    if char.isdigit() == True:
                        #Add the string to the buffer if it's a digit. Print letter but don't progress to new line (end='').
                        #print(char, end="")
                        numBuf += char
                    
                    # Check for minus keypress. Only add if at beginning of number.
                    elif char == '-':
                        if len(numBuf) == 0:
                            #print(char,end="")
                            numBuf += char
                        else:
                            pass
                    
                    # Check for decimal point
                    elif char == '.':
                        #print(char, end="")
                        numBuf += char
                    # Check for backspace or delete keys, remove one from buffer if not only character.
                    elif char in {'\b', '\x08', '\x7F'}:
                        if len(numBuf) == 0:
                            pass
                        elif len(numBuf) > 0:
                            numBuf = numBuf[:-1]
                            #print('\n', numBuf, end="")
                        else:
                            pass
                    
                    # Check for enter key. If buffer isn't empty, empty and write to numIn value for use elsewhere. 
                    elif char =='\r':
                            
                            
                            numIn = float(numBuf)                                
                            numBuf = ''

                            print(f'Data entry accepted: {numIn}')
                            print('')
                            state = S4_GAIN
                    else:
                            pass
                        
                        
            #State 4: Read buffer into appropriate share.            
            elif state == S4_GAIN:
                if KEYFLAG.read() in {'p','n'}:
                    KPSHARE.write(numIn)
                elif KEYFLAG.read() in {'d','m'}:
                    KDSHARE.write(numIn)
                elif KEYFLAG.read() == None:
                    state = S1_WAIT
                    
# =============================================================================
#                 elif KEYFLAG.read() == 'a':
#                     KDSHARE.write(numIn)
#                 elif KEYFLAG.read() == 'b':
#                     KDSHARE.write(numIn)
# =============================================================================
            
            
            
            yield state
        else:
            
            yield None
            
                