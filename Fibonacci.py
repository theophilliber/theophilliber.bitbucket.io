"""
Created on Thu Jan  6 12:42:55 2022

@author: RUODOLF RUMBAOA
"""

def fib():
    stop = 0
    while stop == 0:
        idx = input('Please enter your desired index: ' )
        #checks if input idx is made up of numbers
        if int(idx) >= 0:
            # Using a formula approach
            # fnum is calculating the fibonacci 
            # number at index given. 
            fnum = (((((1 + 2.23606797749979)/2))**abs(int(idx))) - (((1 - 2.23606797749979)/2)**abs(int(idx))))/2.23606797749979
            print(f'The fibonacci number at that index is {int(fnum)}.')
        else:
            print('Index must be an integer!')
        # User is prompted to continue or not
        my_input = input('Would you like to keep going? Y or N?: ')
        #loop is broken when user inputs n or N to prompt
        if my_input == 'N' or my_input == 'n':
            print('Goodbye.')
            break
        else:
            continue
    return 0
if __name__ =='__main__':
    fib()
