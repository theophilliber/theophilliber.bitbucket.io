'''!@file      TaskUser3.py
    @brief      User Interface for Encoder FSM. 
    @details    The task uses the USB VCP (Virtual COM Port) to cooperatively 
                take character input from the user working at a serial terminal
                such as PuTTY. Using the keyboard, the user can perform the following
                functions: zero encoder 1 (z), print position of encoder (p), 
                print delta for encoder (d), collect data for 30 seconds and print
                to PuTTY as comma separated list (g), or end data collection
                early (s). 
    @author     Theo Philliber
    @author     Ruodolf Rumbaoa
    @date       02/03/2022
'''

from time import ticks_us, ticks_add, ticks_diff
import micropython, pyb, shares



S0_INIT = micropython.const(0)
S1_WAIT = micropython.const(1)
S2_ZERO = micropython.const(2)
S3_POS = micropython.const(3)
S4_DELTA = micropython.const(4)
S5_RECORD = micropython.const(5)
S6_CSV = micropython.const(6)
S7_VELO = micropython.const(7)
S8_DUTY1 = micropython.const(8)
S9_FAULT = micropython.const(9)
S10_TEST = micropython.const(10)
S11_DUTY2 = micropython.const(11)
S12_TEST_REC = micropython.const(12)
S13_TEST_PRINT = micropython.const(13)



def taskUserFcn(taskName, period, zFlag, pFlag, dFlag, gFlag, sFlag, vFlag, mFlag, MFlag, cFlag, tFlag, dataList, timeList, velList, printShare, recDone, duty1, duty2): 


    '''!@brief              Generator function to run the UI task as an FSM.
        @details            The task runs as a generator function and requires a
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param zFlag        Boolean variable (shares class) representing if the
                            'z' key has been pressed.
        @param pFlag        Boolean variable (shares class) representing if the
                            'p' key has been pressed.
        @param dFlag        Boolean variable (shares class) representing if the
                            'd' key has been pressed.
        @param gFlag        Boolean variable (shares class) representing if the
                            'g' key has been pressed.
        @param sFlag        Boolean variable (shares class) representing if the
                            's' key has been pressed.
        @param vFlag        Boolean variable (shared class) representing if the 
                            'v' key has been pressed.
        @param mFlag        Boolean variable (shared class) representing if the 
                            'm' key has been pressed.
        @param MFlag        Boolean variable (shared class) representing if the 
                            'M' key has been pressed.
        @param cFlag        Boolean variable (shared class) representing if the 
                            'c' key has been pressed.
        @param tFlag        Boolean variable (shared class) representing if the 
                            't' key has been pressed.
        @param dataList     Shares class used for printing comma separated list
                            of collected data. 
        @param printShare   Shares class used for storing and printing data from
                            position and delta functions called by taskEncoder.py.
        @param recDone      Boolean (shares class) telling whether recording has
                            finished or not.
                            
                            
    '''
    
    # Start at State 0, initialize welcome message with instructions
    state = S0_INIT
    print('+---------------------------------------+')
    print('+       ME305 LAB 3 DEMO                +')
    print('+      BY THEO PHILLIBER AND            +')
    print('+        RUODOLF RUMBAOA                +')
    print('+---------------------------------------+')
    print('+z: Zeroes the position of encoder 1    +')
    print('+p: Prints out the position of encoder 1+')
    print('+d: Prints out delta for encoder 1      +')
    print('+v: Prints out angular velocity for      ')
    print('+   encoder 1                           +')
    print('+c: Clear a fault condition             +')
    print('+g: Get data for 30 seconds and print to+')
    print('+   PuTTY as comma separated list       +')
    print('+s: End data collection permanently     +')
    print('+m: Set duty cycle of motor 1           +')
    print('+M: Set duty cycle of motor 2           +')
    print('+t: Starts a testing interface and      +')
    print('+   prints the average angular velocity +')
    print('+   at chosen duty cycles               +')
    print('+---------------------------------------+')
    
    # Create virtual serial port object for getting keyboard input.
    ser = pyb.USB_VCP()
    
    # Transition to State 1: Wait for input. 
    state = S1_WAIT
    
    # Enter while loop to continually check for keyboard input. Upon input from
    # user, the state will change and the corresponding flag will raise. Upon
    # the next iteration of the taskEncoder.py function, the flag will be read,
    # and the method from encoder.py will be called.
    
    # Establish start time to control rate at which code runs.
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        # Find current time
        current_time = ticks_us()
        # Check if time period has passed since last time code ran.
        if ticks_diff(current_time, next_time) >= 0:
            # Update next_time value for next code iteration.
            next_time = ticks_add(next_time, period)
            
            # State 1: Wait for input. 
            # Check for input, read it (if existing), and raise the correct flag.
            if state == S1_WAIT:
                
                # Check for keyboard input.
                if ser.any():
                    # Read (1 byte) of keyboard input. Raise whatever flag 
                    # corresponds to it.
                    charin = ser.read(1).decode() 
                    print(charin)
                    
                    #Read character, raise corresponding flag and change state.
                    if charin == 'z':
                        zFlag.write(True)
                        state = S2_ZERO
                    elif charin == 'p':
                        pFlag.write(True)
                        state = S3_POS
                    elif charin == 'd':
                       dFlag.write(True)
                       state = S4_DELTA
                    elif charin == 'v':
                       vFlag.write(True)
                       state = S7_VELO
                    elif charin == 'm':
                       print('Enter a duty cycle for motor 1 (-100 to +100) and press enter:')
                       #mFlag.write(True)
                       buf = ''
                       state = S8_DUTY1
                    elif charin == 'M':
                       print('Enter a duty cycle for motor 2 (-100 to +100) and press enter:')
                       #mFlag.write(True)
                       buf = ''
                       state = S11_DUTY2
                    elif charin == 'g': 
                       print('Recording data for 30 seconds. Press the s key to stop recording early. Output will be in a .CSV file')
                       gFlag.write(True)
                       state = S5_RECORD
                    elif charin == 't':
                       print('Beginning velocity testing interface.')
                       tFlag.write(True)
                       testInterval = current_time
                       avgVelArray = [0]
                       dutyArray = [0]
                       buf = ''
                       state = S10_TEST                       
                    elif charin == 's':
                       print('Encoder must be collecting data to stop recording! Press the g key to collect data, and s to stop it.')
                    else:
                        print('Type one of the keys as listed above')
                       
            
            elif state == S2_ZERO:
                if not zFlag.read():
                    print('Encoder zeroed (taskUser)')
                    #zFlag.write(False)
                    state = S1_WAIT
            
            elif state == S3_POS:
                if not pFlag.read():
                    print(f'Encoder Position: {printShare.read()}')
                    state = S1_WAIT
                
            elif state == S4_DELTA:
                if not dFlag.read():
                    print(f'Encoder Delta: {printShare.read()}')    
                    state = S1_WAIT
            
            elif state == S5_RECORD:
                # Check for keyboard input.
                if recDone.read():
                    state = S6_CSV
                    #printNum = 0
                    #dataArray = dataList.read()
                elif ser.any():
                    charin = ser.read(1).decode()  
                    print(charin)
                    if charin == 's':
                        sFlag.write(True)    
            
            elif state == S6_CSV:
                #if printNum < (len(dataArray) - 1):
                if dataList.num_in() > 0:
                    print(f"{timeList.get()}, {dataList.get()}, {velList.get()}")
                    #printNum += 1
                else: 
                    recDone.write(False)
                    print('Recording Finished!')
                   # print(dataList.read())
                    state = S1_WAIT

            elif state == S7_VELO:
                if not vFlag.read():
                    print(f'Encoder Velocity: {printShare.read()}')
                    state = S1_WAIT
                
            elif state == S8_DUTY1:
               
                if ser.any():
                    char = ser.read(1).decode()
                    if char.isdigit() == True:
                        #Add the string to the buffer if it's a digit
                        print(char)
                        buf += char
                    #elif len(buf) == 0:
                    elif char == '-':
                        if len(buf) == 0:
                            
                            buf += char
                        else:
                            pass
                            
                    elif char in {'\b', '\x08', '\x7F'}:
                        if len(buf) == 0:
                            pass
                        elif len(buf) > 0:
                            buf = buf[:-1]
                        else:
                            state = S8_DUTY1
                    
                    elif char =='\r':
                        
                        print(float(buf))
                        duty1.write(float(buf))
                        mFlag.write(True)
                        
                        buf = ''
                        
                        state = S1_WAIT
                    else:
                        state = S8_DUTY1
                    
                        #state = S1_WAIT 
                
                
                    
                        #state = S1_WAIT
                                
                        #elif len(buf) >= 0:
                            #if char == '\b':
                                #buf.pop()
                            #elif char == ('\r' or '\n'):
                                #printShare.write(float(buf))
                                #print(float(buf))
                    
                        
            elif state == S11_DUTY2:
                #elif MFlag.read():
                if ser.any():
                    char = ser.read(1).decode()
                    if char.isdigit() == True:
                        #Add the string to the buffer if it's a digit
                        print(char)
                        buf += char
                    #elif len(buf) == 0:
                    elif char == '-':
                        if len(buf) == 0:
                            buf += char
                        else:
                            pass
                    elif char in {'\b', '\x08', '\x7F'}:
                        if len(buf) == 0:
                            pass
                        elif len(buf) > 0:
                            buf = buf[:-1]
                        else:
                            state = S11_DUTY2        
                    
                    elif char =='\r':
                        #MFlag.write(False)
                        print(float(buf))
                        duty2.write(float(buf))
                        #mFlag.write(False)
                        MFlag.write(True)
                        buf = ''
                        state = S1_WAIT
                    else:
                        state = S11_DUTY2
                
            elif state == S10_TEST:
                """
                if ticks_diff(current_time, next_time) >= 0:
                    # Add five second interval for test data collection
                    next_time = ticks_add(next_time, 5_000_000)
                """    
                if ser.any():
                    char = ser.read(1).decode()
                    if char.isdigit() == True:
                        #Add the string to the buffer if it's a digit
                        print(char, end="")
                        buf += char
                    #elif len(buf) == 0:
                    elif char == '-':
                        if len(buf) == 0:
                            print(char, end="")
                            buf += char
                        else:
                            pass
                    elif char in {'\b', '\x08', '\x7F'}:
                        if len(buf) == 0:
                            pass
                        elif len(buf) > 0:
                            #print("\r", buf, end="\r")
                            buf = buf[:-1]
                        else:
                            state = S10_TEST       
                    
                    elif char =='\r':
                        #MFlag.write(False)
                        #print(float(buf))
                        print("")
                        duty1.write(float(buf))
                        #mFlag.write(False)
                        mFlag.write(True)
                        dutyArray.append(buf)
                        buf = ''
                        testStart = current_time
                        avgVel = []
                        timeDif = 0
                        printShare.write(0)
                        state = S12_TEST_REC
                    elif char == 's':
                        state = S13_TEST_PRINT
                    else:
                        state = S10_TEST
            
            elif state == S12_TEST_REC:
                if ticks_diff(current_time, testStart + 5_000_000) >= 0:
                #if (ticks_diff(current_time, testStart + 5_000_000) >= 0 and ticks_diff(current_time, testStart + 5_000_000)%1000):
                    avgVelArray.append(sum(avgVel)/len(avgVel))
                    print("Enter a duty cycle, or press s to quit")
                    state = S10_TEST
                elif not vFlag.read():
                    vFlag.write(True)
                    avgVel.append(printShare.read())
                    
                else:
                    pass
                    
                
            elif state == S13_TEST_PRINT:
                if len(dutyArray) > 0:
                    print(f"{dutyArray.pop()}, {avgVelArray.pop()}")
                else:
                    state = S1_WAIT
            
            yield state
        else:
            
            yield None
            
                