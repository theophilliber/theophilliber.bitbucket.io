'''!@file      taskUser5.py
    @brief      User Interface for Encoder FSM. 
    @details    The task uses the USB VCP (Virtual COM Port) to cooperatively 
                take character input from the user working at a serial terminal
                such as PuTTY. Using the keyboard, the user can perform the following
                functions: zero encoder 1 (z), print position of encoder (p), 
                print delta for encoder (d), collect data for 30 seconds and print
                to PuTTY as comma separated list (g), or end data collection
                early (s). 
    @author     Theo Philliber
    @author     Ruodolf Rumbaoa
    @date       02/2/2022
'''

from time import ticks_us, ticks_add, ticks_diff
import micropython, pyb 



S0_INIT = micropython.const(0)
S1_WAIT = micropython.const(1)
S2_ZERO = micropython.const(2)
S3_POS = micropython.const(3)
S4_DELTA = micropython.const(4)
S5_RECORD = micropython.const(5)
S6_CSV = micropython.const(6)
S7_VELO = micropython.const(7)
S8_DUTY1 = micropython.const(8)
S9_FAULT = micropython.const(9)
S10_TEST = micropython.const(10)
S11_DUTY2 = micropython.const(11)
S12_TEST_REC = micropython.const(12)
S13_TEST_PRINT = micropython.const(13)
S14_SETGAIN = micropython.const(14)
S15_SETPOINT = micropython.const(15)
S16_STEP_INIT = micropython.const(16)
S17_STEP_TEST = micropython.const(17)
S18_STEP_PRINT = micropython.const(18)

#duty1 = shares.Share()
#duty2 = shares.Share()

def taskUserFcn(taskName, period, keyFlag, taskFin, valueShare, velShare, posShare, timeShare, CLC, rollShare, pitchShare):



    '''!@brief              FSM code that runs user interface.
        @details            The task reads user key inputs and raises the corresponding flag.
                            Flags rasied are read read by motor or IMU to perform desired function.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param keyFlag      Flag containing value of user inputted key command
        @param taskFin      Share telling if task has finished across task files,
                            similar to acknowledge bit in I2C communication
        @param valueShare   Share holding value to transport across tasks. (Duty
                            cycles, gain values, etc.)
        @param velShare     Share holding current value of velocity.
        @param posShare     Share holding current value of encoder position.
        @param timeShare    Share holding current value of time. Could be calculated
                            in each task, but more accurate to find at time of pos/vel data.
        @param CLC          Share telling whether closed-loop control is on or off. By
                            default, the motor is in open-loop control (CLC = False)
        @param rollShare    Share that stores roll angle of the platform from IMU readings
        @param pitchShare   Share that stores pitch angle of the platform from IMU readings
        
                            
                            
    '''
    
    # Start at State 0, initialize welcome message with instructions
    state = S0_INIT
    print('+---------------------------------------+')
    print('+       ME305 LAB 3 DEMO                +')
    print('+      BY THEO PHILLIBER AND            +')
    print('+        RUODOLF RUMBAOA                +')
    print('+---------------------------------------+')
    print('+z: Zeroes the position of encoder 1    +')
    print('+p: Prints out the position of encoder 1+')
    print('+d: Prints out delta for encoder 1      +')
    print('+v: Prints out angular velocity for      ')
    print('+   encoder 1                           +')
    print('+c: Clear a fault condition             +')
    print('+g: Get data for 30 seconds and print to+')
    print('+   PuTTY as comma separated list       +')
    print('+s: End data collection permanently     +')
    print('+m: Set duty cycle of motor 1           +')
    print('+M: Set duty cycle of motor 2           +')
    print('+t: Starts a testing interface and      +')
    print('+   prints the average angular velocity +')
    print('+   at chosen duty cycles               +')
    print('+y: Choose a setpoint speed (rad/s)     +')
    print('+k: Set a gain Kp for the controller    +')
    print('+w: Enable/disable closed-loop control  +')
    print('+r: Starts a testing interface and      +')
    print('+   prints the velocity data for the    +')
    print('+   step response of the motor          +')
    print('+---------------------------------------+')
    
    # Create virtual serial port object for getting keyboard input.
    ser = pyb.USB_VCP()
    
    # Transition to State 1: Wait for input. 
    state = S1_WAIT
    
    # Enter while loop to continually check for keyboard input. Upon input from
    # user, the state will change and the corresponding flag will raise. Upon
    # the next iteration of the taskEncoder.py function, the flag will be read,
    # and the method from encoder.py will be called.
    
    # Establish start time to control rate at which code runs.
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    # @param numIn      Boolean telling whether numerical user input should be checked.
    numCheck = False
    numBuf = ''
    numIn = None
    
    while True:
        # Find current time
        current_time = ticks_us()
        # Check if time period has passed since last time code ran.
        if ticks_diff(current_time, next_time) >= 0:
            # Update next_time value for next code iteration.
            next_time = ticks_add(next_time, period)
            
           
            
            # If numerical input is needed, check input and process data.
            if numCheck:
                if ser.any():
                    char = ser.read(1).decode()
                    if char.isdigit() == True:
                        #Add the string to the buffer if it's a digit. Print letter but don't progress to new line (end='').
                        print(char, end="")
                        numBuf += char
                    
                    # Check for minus keypress. Only add if at beginning of number.
                    elif char == '-':
                        if len(numBuf) == 0:
                            print(char,end="")
                            numBuf += char
                        else:
                            pass
                    
                    # Check for decimal point
                    elif char == '.':
                        print(char, end="")
                        numBuf += char
                    # Check for backspace or delete keys, remove one from buffer if not only character.
                    elif char in {'\b', '\x08', '\x7F'}:
                        if len(numBuf) == 0:
                            pass
                        elif len(numBuf) > 0:
                            numBuf = numBuf[:-1]
                            print('\n', numBuf, end="")
                        else:
                            pass
                    
                    # Check for enter key. If buffer isn't empty, empty and write to numIn value for use elsewhere. 
                    elif char =='\r':
                            
                            
                            numIn = float(numBuf)                                
                            numBuf = ''
                            print()
                            print(f'Data entry accepted: {numIn}')
                    else:
                            pass
            
            
            
            # State 1: Wait for input. 
            # Check for input, read it (if existing), and raise the correct flag.
            if state == S1_WAIT:
                
                # Check for keyboard input.
                if ser.any():
                    # Read (1 byte) of keyboard input. Raise whatever flag 
                    # corresponds to it.
                    charin = ser.read(1).decode() 
                    print(charin)
                    
                    #Read character, change value of keyFlag and change state.
                    if charin in {'z', 'Z'}:
                        keyFlag.write('z')
                        state = S2_ZERO
                    elif charin in {'p', 'P'}:
                        keyFlag.write('p')
                        state = S3_POS
                    elif charin in {'d', 'D'}:
                        keyFlag.write('d')
                        state = S4_DELTA
                    elif charin in {'v', 'V'}:
                        keyFlag.write('v')
                        state = S7_VELO
                    
                    elif charin == 'm':
                       if not CLC.read():
                           keyFlag.write('m')
                           print('Enter a duty cycle for motor 1 (-100 to +100) and press enter:')
                           numCheck = True
                           state = S8_DUTY1
                       else:
                           print('Motor 1 must be in open-loop control mode (m) to enter a duty cycle.')
                           state = S1_WAIT
                    elif charin == 'M':
                        keyFlag.write('M')
                        numCheck = True
                        print('Enter a duty cycle for motor 2 (-100 to +100) and press enter:')
                        state = S11_DUTY2
                       
                    elif charin in {'k', 'K'}:
                        keyFlag.write('k')
                        print('Enter a proportional gain for closed-loop control:')
                        numCheck = True
                        state = S14_SETGAIN
                    
                    elif charin in {'w', 'W'}:
                        keyFlag.write('w')
                        if CLC.read():
                            CLC.write(False)
                            print('Closed-loop control disabled.')
                        else:
                            CLC.write(True)
                            print('Closed-loop control enabled.')
                    elif charin in {'y', 'Y'}:
                         keyFlag.write('y')
                         numCheck = True
                         print('Enter a target rotational speed (rad/s):')
                         state = S15_SETPOINT
                        
                    elif charin in {'g', 'G'}: 
                         print('Recording data for 30 seconds. Press the s key to stop recording early. Output will be in a .CSV file')
                         keyFlag.write('g')
                         state = S5_RECORD
                    elif charin in {'r', 'R'}:
                         keyFlag.write('r')
                         gain           = None
                         setpoint       = None
                         print('Enter a gain value')
                         dutyList       = None
                         timeList       = None
                         velList        = None
                         state = S16_STEP_INIT
                    elif charin in {'t', 'T'}:
                        print('Beginning velocity testing interface.')
                        keyFlag.write('t')
                        avgVelArray = [0]
                        dutyArray = [0]
                        buf = ''
                        state = S10_TEST                       
                    elif charin in {'s', 'S'}:
                        print('Encoder must be collecting data to stop recording! Press the g key to collect data, and s to stop it.')
                    else:
                        print('Type one of the keys as listed above.')
                       
            
            
            elif state == S2_ZERO:
                if taskFin.read():
                    print('Encoder zeroed (taskUser)')
                    taskFin.write(False)
                    state = S1_WAIT
            
            elif state == S3_POS:
                if taskFin.read():
                    print(f'Encoder Position: {posShare.read()}')
                    taskFin.write(False)
                    state = S1_WAIT
                
            elif state == S4_DELTA:
                if taskFin.read():
                    print(f'Encoder Delta: {valueShare.read()}')   
                    valueShare.write(None)
                    taskFin.write(False)
                    state = S1_WAIT
                    
            
            elif state == S16_STEP_INIT:
                
                if not gain:
                     numCheck = True
                     if numIn != None:
                         print('Enter a target velocity:')
                         gain = numIn
                         numIn = None
                         numCheck = False
                
                elif not setpoint:
                    numCheck = True
                    if numIn != None:
                        setpoint = numIn
                        numIn = None
                        numCheck = False
                else:
                    valueShare.write([gain, setpoint])
                    if taskFin.read():
                        gain = None
                        setpoint = None
                        i = 0
                        state = S17_STEP_TEST
                        taskFin.write(False)
                 
            
            elif state == S17_STEP_TEST:
                if taskFin.read():
                   if valueShare.read() != None:
                       if dutyList == None:
                           dutyList = valueShare.read()
                           print('dutylist acknowledged')
                           valueShare.write(None)
                       elif velList == None:
                           velList = valueShare.read()
                           print('velList ackd')
                           valueShare.write(None)
                       elif timeList == None:
                           timeList = valueShare.read()
                           print('timeList ackd')
                           valueShare.write(None)
                           state = S18_STEP_PRINT
                       #else:
                       #    print('heading to state 18')
                       #    state = S18_STEP_PRINT
                           
# =============================================================================
#                         elif i < len(timeList):
#                            print(f'{timeList[i]}, {velList[i]}, {dutyList[i]}')
# =============================================================================
# =============================================================================
#                        else:
#                            state = S1_WAIT
#                            dutyList = 0
#                            timeList = 0
#                            velList = 0
#                            taskFin.write(False)
# =============================================================================
                       
            elif state == S18_STEP_PRINT:
                if i < len(timeList):
                    
                    print(f'{timeList[i]/1000000}, {velList[i]}, {dutyList[i]}')
                    
                    
                    i += 1
                else:
                    state = S1_WAIT
                    dutyList = 0
                    timeList = 0
                    velList = 0
                    taskFin.write(False)
                           
                        
                    
            
            
            elif state == S14_SETGAIN:
                                
                if numIn != None:
                    valueShare.write(numIn)
                    numIn = None
                    numCheck = False
                    
                if taskFin.read():
                    taskFin.write(False)
                    state = S1_WAIT
                
            
            elif state == S15_SETPOINT:
                                
                if numIn != None:
                    valueShare.write(numIn)
                    numIn = None
                    numCheck = False
                    
                if taskFin.read():
                    taskFin.write(False)
                    state = S1_WAIT
                
            
            elif state == S5_RECORD:
                # Check for keyboard input.
                if taskFin.read():
                    #dataList, velList, timeList = valueShare.read()
                    valueShare.write(None)
                    state = S6_CSV
                    prevPos = 0
                    prevTime = 0
                    taskFin.write(False)
                    print("Time [s], Position [ticks], Velocity [rad/s]")
                    #printNum = 0
                    #dataArray = dataList.read()
                elif ser.any():
                    charin = ser.read(1).decode()  
                    print(charin)
                    if charin in {'s', 'S'}:
                        keyFlag.write('s')    
            
            elif state == S6_CSV:
                #if printNum < (len(dataArray) - 1):
                
                if valueShare.read():
                    timek, posk = valueShare.read()
                    timek = float("{:.2f}".format(timek/1_000_000))
                    velk = float("{:.2f}".format((posk - prevPos)*(2*3.14/(4000*(timek-prevTime)))))
                    print(f'{timek}, {posk}, {velk}')
                    prevTime = timek
                    prevPos = posk
                    valueShare.write(None)
                    
                elif taskFin.read(): 
                    taskFin.write(True)
                    print('Recording Finished!')
                    # print(dataList.read())
                    state = S1_WAIT


            elif state == S7_VELO:
                    print(f'Encoder Velocity: {valueShare.read()}')
                    valueShare.write(None)
                    taskFin.write(False)
                    state = S1_WAIT
            
                
            elif state == S8_DUTY1:
                
                if numIn != None:
                    valueShare.write(numIn)
                    numIn = None
                    numCheck = False
                    
                if taskFin.read():
                    taskFin.write(False)
                    state = S1_WAIT
                
                    
                
                    
                        
            elif state == S11_DUTY2:
                 
                if numIn != None:
                    valueShare.write(numIn)
                    numIn = None
                    numCheck = False
                    
                if taskFin.read():
                    taskFin.write(False)
                    state = S1_WAIT
                
                
                
            elif state == S10_TEST:
                """
                if ticks_diff(current_time, next_time) >= 0:
                    # Add five second interval for test data collection
                    next_time = ticks_add(next_time, 5_000_000)
                """    
                if ser.any():
                    char = ser.read(1).decode()
                    if char.isdigit() == True:
                        #Add the string to the buffer if it's a digit
                        print(char, end="")
                        buf += char
                    #elif len(buf) == 0:
                    elif char == '-':
                        if len(buf) == 0:
                            print(char, end="")
                            buf += char
                        else:
                            pass
                    elif char in {'\b', '\x08', '\x7F'}:
                        if len(buf) == 0:
                            pass
                        elif len(buf) > 0:
                            #print("\r", buf, end="\r")
                            buf = buf[:-1]
                        else:
                            state = S10_TEST       
                    
                    elif char =='\r':
                        #MFlag.write(False)
                        #print(float(buf))
                        print("")
                        duty1.write(float(buf))
                        #mFlag.write(False)
                        mFlag.write(True)
                        dutyArray.append(buf)
                        buf = ''
                        testStart = current_time
                        avgVel = []
                        timeDif = 0
                        printShare.write(0)
                        state = S12_TEST_REC
                    elif char in {'s', 'S'}:
                        state = S13_TEST_PRINT
                    else:
                        state = S10_TEST
            
            elif state == S12_TEST_REC:
                if ticks_diff(current_time, testStart + 5_000_000) >= 0:
                    avgVelArray.append(sum(avgVel)/len(avgVel))
                    print("Enter a duty cycle, or press s to quit")
                    state = S10_TEST
                elif not vFlag.read():
                    vFlag.write(True)
                    avgVel.append(velShare.read())
                    
                else:
                    pass
                    
                
            elif state == S13_TEST_PRINT:
                if len(dutyArray) > 0:
                    print(f"{dutyArray.pop()}, {avgVelArray.pop()}")
                else:
                    state = S1_WAIT
                    
            
            yield state
        else:
            
            yield None
            
                