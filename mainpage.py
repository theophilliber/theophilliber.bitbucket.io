"""!@file mainpage.py
@mainpage ME305 LABS 
@brief  This site contains labs performed for ME305 Inroduction to Mechatronics.
        Each lab has been recorded within their own subpage (links located below):
@n
@subpage lab1 "Lab 0x01: Getting Started with Hardware"
@n
@subpage lab2 "Lab 0x02: Quadrature Encoders"
@n
@subpage lab3 "Lab 0x03: PMDC Motors"
@n
@subpage lab4 "Lab 0x04: Closed Loop Control"
@n
@subpage lab5 "Lab 0x05: I2C and IMU"
@n
@subpage hw2 "HW2 : Modeling Motor and Platform (2D)"
@n
@subpage hw3 "HW3 : Simulation of 2D Ball and Platform Model"
@n
@subpage TP "Term Project"
"""

