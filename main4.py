"""!@file main4.py
@brief      Main file for lab 4. Impleneted closed loop control of the motor.
@n
@page       lab4 Lab 0x04: Closed Loop Control
@brief      Impleneted closed loop control of the motor.
@details    This code reads input from a quadrature incremental encoder. There is a 
            user interface (taskUser.py) which allows the user to control the 
            encoder to perform several functions. It also prints out a description
            of what the functions are and how to execute them. The file taskEncoder.py
            acts as an interface between the UI file and the encoder driver (encoder.py),
            calling methods from the encoder and passing them to the UI to print.
            The driver file sets up an encoder class and creates the methods used
            by the other files. Additionally, this code includes a closed-loop 
            proportional controller which can be set up using user inputs from
            the user interface.
            
    
@image html TransitionDiagram4.png
@image html motorstepresponse.png width=800px height=450px
@n
This graph shows the data collected for the motor when inputing a reference velocity of 
100 rad/s and a proportional gain of Kp = 1.
         
Source Folder
    [link]
Demo Video
    [Link]

@author Theo Philliber
@author Ruodolf Rumbaoa
@date 02/17/2022
"""
import taskUser4, taskEncoder4, shares, taskMotor4, array

if __name__ == '__main__':
    
    # Share definitions:
    # String shares:
    keyFlag = shares.Share(None)
    
    # Boolean shares:
    taskFin = shares.Share(False)
    CLC = shares.Share(False)
    
    # Numerical shares:
    valueShare = shares.Share(None)
    velShare = shares.Share(0)
    posShare = shares.Share(0)
    timeShare = shares.Share(0)
    
    # Run tasks one by one
    taskList = [taskUser4.taskUserFcn('Task User', 10_000, keyFlag, taskFin, valueShare, velShare, posShare, timeShare, CLC), 
                taskEncoder4.taskEncFcn('Task Encoder', 10_000, keyFlag, taskFin, valueShare, velShare, posShare, timeShare, CLC),
                taskMotor4.taskMotorFnc('Task Motor', 10_000, keyFlag, taskFin, valueShare, velShare, posShare, timeShare, CLC)]

    
    while True:
        
        try:
            for task in taskList:
                
              
                next(task)
           
        
        except KeyboardInterrupt:
        
            break
    print('Program Terminating')

