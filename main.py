"""!@file main.py
@page TP  Term Project: Ball Balancing
@brief Platform that balances a ball.
@details The goal of this term project was to build a platform that will balance
a ball on its surface. It utilizes motors to actuate the platform, and receives 
feedback in the form of IMU data and a touch panel. The IMU measures the angular
orientation and rotation of the platform, while the touch panel measures the location
of the ball. They are combined into a PD cascade controller to provide duty cycles
for the motors.
@n

            

         
@image html TaskDiagram.png
@image html taskMotorFSM.png
@image html taskUserFSM.png
@image html taskDataCollectFSM.png 
@image html taskIMUFSM.png
@image html taskTouchPanelFSM.png


Source Folder
    [https://bitbucket.org/ruodolfr/me305_lab/src/master/]

@author Theo Philliber
@author Ruodolf Rumbaoa
@date 03/18/2022

"""
import taskUser, taskIMU, taskTouchPanel, taskMotor, shares
#taskDataCollect, taskMotor, shares
import taskUser, taskTouchPanel, shares
from micropython import const

if __name__ == '__main__':
    
    period          = const(10000)
    #periodTP (touch panel) for running taskTouchPanel at different rate, allowing for multiple data samples per iteration
    periodTP        = const(10000)
    
    
    
    # Share definitions:
    # String shares:
    KEYFLAG         = shares.Share(None)
    
    # Boolean shares:
    TASKFIN         = shares.Share(False)
    ENABLE          = shares.Share(False)
    
    # Numerical shares:
    BALLPOS         = shares.Share((0,0,0))
    BALLVEL         = shares.Share((0,0))
    KPSHARE         = shares.Share(None)
    KDSHARE         = shares.Share(None)
    
    
    
    #valueShare = shares.Share(None)
    velShare = shares.Share(0)
    posShare = shares.Share(0)
    timeShare = shares.Share(0)
    
    rollShare = shares.Share(0)
    pitchShare = shares.Share(0)
    rollvShare = shares.Share(0)
    pitchvShare = shares.Share(0)
    
    # Run tasks one by one
    taskList = [taskUser.taskUserFcn('Task User', period, ENABLE, KEYFLAG, TASKFIN, KPSHARE, KDSHARE),
                taskIMU.taskIMUFcn('Task IMU', period, TASKFIN, rollShare, pitchShare, rollvShare, pitchvShare),
                taskTouchPanel.taskTouchPanelFcn('Task Touch Panel', periodTP, KEYFLAG, TASKFIN, BALLPOS, BALLVEL),
                #taskDataCollect.taskDataCollectFcn('Task Data', period,),
                taskMotor.taskMotorFcn('Task Motor', period, ENABLE, KEYFLAG, TASKFIN, KPSHARE, KDSHARE, BALLPOS, BALLVEL, rollShare, pitchShare, rollvShare, pitchvShare)]

    
    
    while True:
        
        try:
            for task in taskList:
                
              
                next(task)
           
        
        except KeyboardInterrupt:
        
            break
    print('Program Terminating')

