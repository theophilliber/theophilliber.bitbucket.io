'''!@file      taskMotor5.py
    @brief     Task responsible for motor driving. 
    @details   
    @author     Theo Philliber
    @author     Ruodolf Rumbaoa
    @date       02/2/2022
'''
from time import ticks_us, ticks_add, ticks_diff
import array
from pyb import Timer, Pin
import micropython

from motor5 import Motor
from closedLoop5 import ClosedLoop

S0_INIT = micropython.const(0)
S1_WAIT = micropython.const(1)
S2_STAY = micropython.const(2)
S3_CLR = micropython.const(3)
S4_TEST = micropython.const(4)
S5_CSV = micropython.const(5)
S17_STEP_TEST = micropython.const(17)
#duty1 = shares.Share()
#duty2 = shares.Share()



#def taskMotorFnc(taskName, period, vFlag, mFlag, MFlag, cFlag, gFlag, tFlag, 
#                 sFlag, yFlag, duty1, duty2, kp, wFlag, kFlag, velShare, v_ref):
 
def taskMotorFnc(taskName, period, keyFlag, taskFin, valueShare, velShare, posShare, timeShare, CLC, rollShare, pitchShare):
    '''!@brief              FSM code that runs user interface.
        @details            The task reads user key inputs and raises the corresponding flag.
                            Flags rasied are read read by motor or IMU to perform desired function.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param keyFlag      Flag containing value of user inputted key command
        @param taskFin      Share telling if task has finished across task files,
                            similar to acknowledge bit in I2C communication
        @param valueShare   Share holding value to transport across tasks. (Duty
                            cycles, gain values, etc.)
        @param velShare     Share holding current value of velocity.
        @param posShare     Share holding current value of encoder position.
        @param timeShare    Share holding current value of time. Could be calculated
                            in each task, but more accurate to find at time of pos/vel data.
        @param CLC          Share telling whether closed-loop control is on or off. By
                            default, the motor is in open-loop control (CLC = False)
        @param rollShare    Share that stores roll angle of the platform from IMU readings
        @param pitchShare   Share that stores pitch angle of the platform from IMU readings
        
                            
                            
    '''
    state = S0_INIT
    PWM_tim = Timer(3, freq = 20_000)
    
     
    motor_1 = Motor(PWM_tim, 1, 2, Pin.cpu.B4, Pin.cpu.B5)
    motor_2 = Motor(PWM_tim, 3, 4, Pin.cpu.B0, Pin.cpu.B1)
     
     
    rollDuty = 0
    pitchDuty = 0     
    PID = ClosedLoop(80, 80)
    rollsetpoint = 0.011
    pitchsetpoint = 0.52

     #.enable(motor_1)
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    state = S1_WAIT
    while True:
        # Find current time
        current_time = ticks_us()
        # Check if time period has passed since last time code ran.
        if ticks_diff(current_time, next_time) >= 0:
            # Update next_time value for next code iteration.
            next_time = ticks_add(next_time, period)
    
    
            if state == S1_WAIT:
                if taskFin.read():
                    state = S2_STAY
                    taskFin.write(False)
                
                else:
                    pass
        
                
            elif state == S2_STAY:
                #myEncoder1.update()
                
                #if CLC.read():
                print(rollShare.read(), pitchShare.read())
                rollDuty = PID.update(rollsetpoint, rollShare.read())
                
                motor_1.set_duty(rollDuty)
                
                pitchDuty = PID.update(pitchsetpoint, pitchShare.read())
                motor_2.set_duty(pitchDuty)
                
                pass
                    
                    
                if keyFlag.read() == 'r':
                    
                    if valueShare.read():
                        taskFin.write(True)
                        gain, setpoint = valueShare.read()
                        valueShare.write(None)
                        keyFlag.write(None)
                        PID.set_gain(gain, 1)
                        testStart = current_time
                        dutyList = array.array('f', 305*[0])
                        i = 0
                        state = S17_STEP_TEST
           
# =============================================================================
#             
#                 elif cFlag.read():
#                     #print('Recording data for 30 seconds. Press the s key to stop recording early. Output will be in a .CSV file')
#                     #record_start_time = ticks_us()
#                     pass
#                     #timeArray = array.array('l', 3001*[0])
#                     #posArray = array.array('l', 3001*[0])
#                     #i = 0
#                     #state = S5_RECORD
#                 elif gFlag.read():
#                     #print('Getting delta of encoder reading...')
#                     #printShare.write(myEncoder1.get_delta())
#                     gFlag.write(False)
#                     state = S1_WAIT
#                     
#                 elif tFlag.read():
#                     #print('Getting delta of encoder reading...')
#                     #printShare.write(myEncoder1.get_delta())
#                     tFlag.write(False)
#                     state = S1_WAIT
# =============================================================================
                
                #Set gain Kp for PID controller
                elif keyFlag.read() == 'k':
                    if valueShare.read() != None:
                        PID.set_gain(valueShare.read(), 1)
                        valueShare.write(None)
                        keyFlag.write(None)
                        taskFin.write(True)
                        state = S1_WAIT
               
                #Set setpoint, reference speed for PID
                elif keyFlag.read() == 'y':
                    if valueShare.read() != None:
                        setpoint = valueShare.read()
                        valueShare.write(None)
                        keyFlag.write(None)
                        taskFin.write(True)
                        state = S1_WAIT
                    
                
            elif state == S17_STEP_TEST:
                
                if ticks_diff(current_time, testStart + 1_000_000) <= 0:
                    motor_1.set_duty(0)
                    dutyList[i] = 0
                    i += 1
                elif ticks_diff(current_time, testStart + 3_000_000) <= 0:
                    motorDuty1 = PID.update(setpoint, velShare.read())
                    motor_1.set_duty(motorDuty1)
                    dutyList[i] = motorDuty1
                    i += 1
                else:
                    motor_1.set_duty(0)
                    PID.set_gain(0, 1)
                    valueShare.write(dutyList)
                    taskFin.write(True)    
                    state = S1_WAIT
                        
                
                
                        
                    
                
                    
                    
            #elif state == S5_RECORD:
                
             #   if i <= len(timeArray):
              #      myEncoder1.update()
                   
               #     if i % 1000 == 0:
                #        timeArray[i], posArray[i] = (current_time - record_start_time, myEncoder1.get_position())
                #    i += 1
                    
                #if ticks_diff(ticks_us(), record_start_time) >= 3000000:
               # if sFlag.read():
                #    dataList.write(posArray)
                 #   recDone.write(True)
                  #  sFlag.write(False)
                   # gFlag.write(False)
                    #state == S1_WAIT
                #elif current_time > record_start_time + 30_000_000:
                 #   dataList.write(posArray)
                  #  recDone.write(True)
                   # sFlag.write(False)
                    #gFlag.write(False)
                    #state == S1_WAIT
                    
               # else:                    
                    #myEncoder1.update()
                    #data_Array[i] = myEncoder1.get_position();
                    #if i % 10 == 0: 
                     #   data_Array.append(myEncoder1.get_position())
                    #i += 1
                                
    
            
            
            yield state
        else:
            yield None