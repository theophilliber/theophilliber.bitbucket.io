"""!@file Lab0x01.py
@brief  Main file for lab 1. Used familiarize with hardware.
@page lab1 Lab 0x01: Getting Started with Hardware
@brief Program that runs three blinking patterns on an LED.
@n
@details This program displays three different blinking patterns on an LED 
         located on the Nucleo-L476RG board. The program begins by displaying 
         a welcome message. Through a button on the board, the user can cycle 
         through the blinking patterns. They are all repeating waveforms: a 
         square wave, a sine wave, and a sawtooth wave. The program will run 
         until the user prompts it to close by pressing CTRL+C on the keyboard.
    
@image html TransitionDiagramlab1.png
@image html ButtonTransitionDiagram.png

Source Folder
    [https://bitbucket.org/ruodolfr/me305_lab/src/master/Lab1/]

Demo Video
    [https://cpslo-my.sharepoint.com/:v:/g/personal/phillibe_calpoly_edu/ETXswSVmpqBIkPvl_Uxohy4BowKos0sQu8SOhJ5c99oEFA?e=h8IGSo]

@author Theo Philliber
@author Ruodolf Rumbaoa
@date 1/20/2022



"""

# import
import pyb, time, math

def onButtonPressFCN(IRQ_src):
    """
    Callback function triggered by external interrupt ButtonInt.
    Changes value of variable buttonToggle to True.    

    Parameters
    ----------
    IRQ_src : 
        Source pin of external interrupt.

    Returns
    -------
    None.

    """
    global buttonToggle
    buttonToggle = True

def SquareWave(timeElapsed):
    """
    Function that takes in the current time elapsed since button press, and
    calculates the brightness of the LED at that time for a square wave.

    Parameters
    ----------
    timeElapsed : INT
        Difference in ms between start time (button press) and current time.

    Returns
    -------
    INT
        Brightness value at current value of timeElapsed for square wave pattern.

    """
    highLow = round(timeElapsed/1000) % 2
    return (1-highLow)*100

def SawWave(timeElapsed):
    
    """
    Function that takes in the current time elapsed since button press, and
    calculates the brightness of the LED at that time for a square wave.

    Parameters
    ----------
    timeElapsed : INT
        Difference in ms between start time (button press) and current time.

    Returns
    -------
    INT
        Brightness value at current value of timeElapsed for sine wave pattern.

    """
    brightness = round(timeElapsed%1000) /10
    return brightness

def SineWave(timeElapsed):
    """
    Function that takes in the current time elapsed since button press, and
    calculates the brightness of the LED at that time for a square wave.

    Parameters
    ----------
    timeElapsed : INT
        Difference in ms between start time (button press) and current time.

    Returns
    -------
    INT
        Brightness value at current value of timeElapsed for sawtooth wave pattern.

    """
    brightness = (math.sin(timeElapsed*2*math.pi/10000)+1)*50
    return brightness


if __name__ == '__main__':
    print('Welcome. Press BLUE BUTTON (B1) to cycle through LED patterns.')
    ## @var state 
    # Current state (0-idle, 1-square, 2-sine, 3-sawtooth) of FSM
    state = 0
    
    ## @var buttonToggle
    # Boolean variable for button press.
    buttonToggle = False
   
    ## @var pinA5
    # Pin A5 representing timer pin on board
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    
    ## @var tim2
    # Timer object, frequency = 1000 Hz
    tim2 = pyb.Timer(2, freq = 1000)
    
    ## @var t2ch1
    # Channel 1 on Timer 2 (tim2), used for PWM timing
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    ## @var pinC13
    # Pin used for lighting LED
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    
    ## @var ButtonInt
    # Interrupt triggered by button press, runs callback function.
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    
    ## @var startTime 
    # Time of start after button press, used for finding elapsed time.
    startTime = time.ticks_ms()
    
    while True:
        
        try:
            
            if state == 0:
                
                if buttonToggle:
                    buttonToggle = False
                    state = 0
                    print(state)
                t2ch1.pulse_width_percent(0)
                
                
            if state == 1:
                
                if buttonToggle:
                    buttonToggle = False
                    state = 1
                    print('Sine Wave')
                    startTime = time.ticks_ms()
                
                ## @var waveformTime 
                # Difference between current time and start time
                waveformTime = time.ticks_diff(time.ticks_ms(), startTime)
                
                ## @var brightness
                # Brightness of LED, ranging from 0 (off) to 100 (fully on)
                brightness = SquareWave(waveformTime)
                t2ch1.pulse_width_percent(brightness)
                
            if state == 2:
                
                if buttonToggle:
                    buttonToggle = False
                    state = 2
                    print('Saw Wave')
                    startTime = time.ticks_ms()
                    
                waveformTime = time.ticks_diff(time.ticks_ms(), startTime)
                brightness = SineWave(waveformTime)
                t2ch1.pulse_width_percent(brightness)
                
                
            
            if state == 3:
                
                if buttonToggle:
                    buttonToggle = False
                    state = 0
                    print('Square Wave')
                    startTime = time.ticks_ms()
                waveformTime = time.ticks_diff(time.ticks_ms(), startTime)
                brightness = SawWave(waveformTime)
                t2ch1.pulse_width_percent(brightness)
            
           

        except KeyboardInterrupt:
            break
    print("Program Terminating")