"""!@file lab2main.py
@brief      Main file for lab 2. Incorporates a user interface to interact with 
            encoder object.
             
@page lab2 Lab 0x02: Incremental Encoders
@brief      Main file for lab 2. Incorporates a user interface to interact with 
            encoder object.
@details    This code reads input from a quadrature incremental encoder. There is a 
            user interface (taskUser.py) which allows the user to control the 
            encoder to perform several functions. It also prints out a description
            of what the functions are and how to execute them. The file taskEncoder.py
            acts as an interface between the UI file and the encoder driver (encoder.py),
            calling methods from the encoder and passing them to the UI to print.
            The driver file sets up an encoder class and creates the methods used
            by the other files.
            
    
@image html 'TransitionDiagram.png'
         
Source Folder
    [link]
Demo Video
    [Link]

@author Theo Philliber
@author Ruodolf Rumbaoa
@date 1/20/2022
"""
import taskUser2, taskEncoder2, shares

if __name__ == '__main__':
    
    # Shares for button presses
    zFlag = shares.Share(False)
    pFlag = shares.Share(False)
    dFlag = shares.Share(False)
    gFlag = shares.Share(False)
    sFlag = shares.Share(False)
    vFlag = shares.Share(False)
    mFlag = shares.Share(False)
    MFlag = shares.Share(False)
    
    #recDone -- flag indicating recording is finished
    recDone = shares.Share(False)
    #dataList - list variable to print out recorded data to PuTTY
    dataList = shares.Queue()
    timeList = shares.Queue()
    #printShare - variable to print position and delta values
    printShare = shares.Share(0)
    
    
    
    taskList = [taskUser2.taskUserFcn('Task User', 10_000, zFlag, pFlag, dFlag, gFlag, sFlag, vFlag, mFlag, MFlag, dataList, timeList, printShare, recDone), 
                taskEncoder2.taskEncFcn('Task Encoder', 10_000, zFlag, pFlag, dFlag, gFlag, sFlag, vFlag, mFlag, MFlag, dataList, timeList, printShare, recDone)]

    
    while True:
        
        try:
            for task in taskList:
                
              
                next(task)
           
        
        except KeyboardInterrupt:
        
            break
    print('Program Terminating')

