'''!@file      TaskMotor3.py
    @brief     Task responsible for motor driving. 
    @details   
    @author     Theo Philliber
    @author     Ruodolf Rumbaoa
    @date       02/02/2022
'''
from time import ticks_us, ticks_add, ticks_diff
#import lab3main, array
from pyb import Timer, Pin
import encoder3, shares, micropython
from DRV8847 import DRV8847
import motor3

S0_INIT = micropython.const(0)
S1_WAIT = micropython.const(1)
S2_DTY = micropython.const(2)
S3_CLR = micropython.const(3)
S4_TEST = micropython.const(4)
S5_CSV = micropython.const(5)
#duty1 = shares.Share()
#duty2 = shares.Share()



def taskMotorFnc(taskName, period, vFlag, mFlag, MFlag, cFlag, gFlag, tFlag, sFlag, duty1, duty2):
    '''!@brief         FSM code that runs the motor task.
        @details         The motor task reads flags raised by the user task and encoder task 
                       and performs the corresponding function.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                       number of microseconds.
                       
        @param zFlag        Boolean variable (shares class) representing if the
                       'z' key has been pressed.
        @param mFlag        Boolean variable (shares class) representing if the
                       'm' key has been pressed.
        @param MFlag        Boolean variable (shares class) representing if the
                       'M' key has been pressed.
        @param cFlag        Boolean variable (shares class) representing if the
                       'c' key has been pressed.
        @param tFlag        Boolean variable (shares class) representing if the
                       't' key has been pressed.
        @param gFlag        Boolean variable (shares class) representing if the
                       'g' key has been pressed.
        @param sFlag        Boolean variable (shares class) representing if the
                       's' key has been pressed.
        @param timeList     Share used to print time array.
        @param velList      Share used for stroing and printing motor velocity
        @param dataList     Shares class used for printing comma separated list
                       of collected data. 
        @param duty1       Stores duty cycle value to apply duty to motor 1
        @param duty2       Stores duty cycle value to apply duty to motor 2
    '''
    state = S0_INIT
    PWM_tim = Timer(3, freq = 20_000)
    
    motor_drv = DRV8847()
    
    motor_1 = motor_drv.Motorf(PWM_tim, 1, 2, Pin.cpu.B4, Pin.cpu.B5)
    motor_2 = motor_drv.Motorf(PWM_tim, 3, 4, Pin.cpu.B0, Pin.cpu.B1)
    motor_drv.enable()
    #.enable(motor_1)
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    state = S1_WAIT
    while True:
        # Find current time
        current_time = ticks_us()
        # Check if time period has passed since last time code ran.
        if ticks_diff(current_time, next_time) >= 0:
            # Update next_time value for next code iteration.
            next_time = ticks_add(next_time, period)
    
                
            if state == S1_WAIT:
                #myEncoder1.update()
                
                if vFlag.read():
                     #print('Encoder zeroing')
                     #myEncoder1.zero(myEncoder1.position)
                     vFlag.write(False)
                     state = S1_WAIT
                     
                elif mFlag.read():
                    #print('Getting encoder position...')
                    #printShare.write(myEncoder1.get_position())
                    #print('Enter a duty cycle for motor 1')
                    #motor_drv.enable()
                    #print(duty1.read())
                    motor_1.set_duty(duty1.read())
                    mFlag.write(False)
                    state = S1_WAIT
                    
                elif MFlag.read():
                    #print('Getting delta of encoder reading...')
                    #printShare.write(myEncoder1.get_delta())
                    #print('Enter a duty cycle for motor 2')
                    motor_2.set_duty(duty2.read())
                    MFlag.write(False)
                    state = S1_WAIT
                
                elif cFlag.read():
                    #print('Recording data for 30 seconds. Press the s key to stop recording early. Output will be in a .CSV file')
                    #record_start_time = ticks_us()
                    pass
                    #timeArray = array.array('l', 3001*[0])
                    #posArray = array.array('l', 3001*[0])
                    #i = 0
                    #state = S5_RECORD
                elif gFlag.read():
                    #print('Getting delta of encoder reading...')
                    #printShare.write(myEncoder1.get_delta())
                    gFlag.write(False)
                    state = S1_WAIT
                    
                elif tFlag.read():
                    #print('Getting delta of encoder reading...')
                    #printShare.write(myEncoder1.get_delta())
                    tFlag.write(False)
                    state = S1_WAIT
                    
                    
                    
            #elif state == S5_RECORD:
                
             #   if i <= len(timeArray):
              #      myEncoder1.update()
                   
               #     if i % 1000 == 0:
                #        timeArray[i], posArray[i] = (current_time - record_start_time, myEncoder1.get_position())
                #    i += 1
                    
                #if ticks_diff(ticks_us(), record_start_time) >= 3000000:
               # if sFlag.read():
                #    dataList.write(posArray)
                 #   recDone.write(True)
                  #  sFlag.write(False)
                   # gFlag.write(False)
                    #state == S1_WAIT
                #elif current_time > record_start_time + 30_000_000:
                 #   dataList.write(posArray)
                  #  recDone.write(True)
                   # sFlag.write(False)
                    #gFlag.write(False)
                    #state == S1_WAIT
                    
               # else:                    
                    #myEncoder1.update()
                    #data_Array[i] = myEncoder1.get_position();
                    #if i % 10 == 0: 
                     #   data_Array.append(myEncoder1.get_position())
                    #i += 1
                                
    
            
            
            yield state
        else:
            yield None