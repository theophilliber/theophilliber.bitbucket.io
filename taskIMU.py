'''!@file taskIMU.py
    @brief Task function file for IMU.
    @details This task file creates a function that will be run in cooperative
    multitasking mode. It runs the IMU driver (BNO055.py).
    It calibrates the IMU, either by reading a text file with the calibration 
    constants or by running the IMU's onboard calibration protocol. Then, it 
    reads the pitch/roll angles and velocities from the sensor.
    @author Theo Philliber
    @author Ruodolf Rumbaoa
    @date 3/18/2022
'''
from pyb import I2C
import BNO055, micropython, os, struct
from time import ticks_us, ticks_add, ticks_diff, sleep

##  @brief S0_INIT
#   @details Initializes taskIMUFcn function. Creates I2C and IMU objects, from pyb.I2C()
#   and BNO055.BNO055(). Configures IMU to IMU (reading) mode, and informs user that calibration sequence
#   is beginning.
S0_INIT = micropython.const(0)
##  @brief S1_CALIB
#   @details Calibration state. Checks if file containing calibration coefficients
#   exists, and writes constants to IMU registers if it does. Otherwise waits for
#   IMU calibration status to be ready, and prompts user to move around platform.
#   Writes constants to text file if it does.
S1_CALIB = micropython.const(1)
##  @brief S2_READ
#   @details Read state. Commands BNO055.py driver to read roll and pitch position
#   and velocity data. Puts values into shares for other tasks to use.
S2_READ = micropython.const(2)




def taskIMUFcn(taskName, period, TASKFIN, rollShare, pitchShare, rollvShare, pitchvShare):
    '''!@brief IMU task function.
        @details Generator function that runs through states. Calibrates sensor,
        then continuously reads sensor data and places values into shares.
        @param taskName Name of task
        @param period Minimum period of time between subsequent runs of function.
        @param TASKFIN Boolean share acknowledging if an action has been completed.
        Used to verify synchronicity between tasks.
        @param rollShare Contains value of roll angle from IMU.
        @param pitchShare Contains value of pitch angle from IMU.
        @param rollvShare Contains value of roll angular velocity from IMU.
        @param pitchvShare Contains value of pitch angular velocity from IMU.
    '''
    
    
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    state = S0_INIT
    bufcal =22*[0]
    
    
    
    while True:
        current_time = ticks_us()
            # Check if time period has passed since last time code ran.
        if ticks_diff(current_time, next_time) >= 0:
            # Update next_time value for next code iteration.
            next_time = ticks_add(next_time, period)
    
            #State 0 INIT
            if state == S0_INIT:
                # Create instance of BNO055 IMU Driver class and I2C communication protocol
                myI2C = I2C(1, I2C.CONTROLLER)
                myIMU = BNO055.BNO055(myI2C)
                myIMU.IMU_mode()





                state = S1_CALIB
                
            
            
    # =============================================================================
    #         try:
    #             file = open('IMU_cal_coeffs.txt', 'r')
    #             cal_coeffs = file.read()
    #             file.close()
    #             
    #         except:
    #             print('file not read')
    #             print('User needs to calibrate the IMU. Move the platform around until interface says it is done calibrating.')
    #             state = 1
    # =============================================================================
                
            elif state == S1_CALIB:
                sleep(2)
                print(f'Accelerometer:{myIMU.calib_status()[0]}, Gyro:{myIMU.calib_status()[1]}')
                
                #print(f'Acc: {myIMU.acc_stat}, GYR: {myIMU.gyr_stat}')
# =============================================================================
#                
#                 # Check if IMU Calibration Coefficient file (IMU_cal_coeffs.txt) exists in pybflash
#                 #if there is then set those values as the constants 
#                 if 'IMU_cal_coeffs.txt' in os.listdir():
#                     with open('IMU_cal_coeffs.txt', 'r') as f:
#                         calDataString = (f.readline())
#                     f.close()
#                     
#                     #Clean irregular values (such as '?') 
#                     calDataClean = ""
#                     for i in calDataString:
#                         if i in {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f',',','x'}:
#                             calDataClean += i
#                     
#                     cal_coeffs = calDataClean.split(',')
#                     res = (map(int, cal_coeffs))
#                     buf = bytearray(22*[0])
#                     struct.pack_into('22B',buf, 0, *res)
#                     myIMU.calib_write(buf)
#                     
#                     
#                     #print(int('0'+cal_coeffs[1],16))
#                     #for i in range(0, len(cal_coeffs)):
#                         #print(hex(int('0'+cal_coeffs[i], 16)))
#                     #    bufcal[i] = hex(int('0'+cal_coeffs[i], 16))
#                     
#                     
#                     #myIMU.calib_write(calDataString)                    
#                     state = S2_READ
#                
# =============================================================================
                
                #If the file is not found, write the file using the values from the calibration procedure
                #Check if calibration statuses are ready
                if (myIMU.acc_stat==3) and (myIMU.gyr_stat==3):
                    TASKFIN.write(True)
                    
                    #Unpack calibration sensor data from addresses as unsigned bytes
                    cal_coeffs = struct.unpack('<22B', (myIMU.calib_read()))
                    #Create/edit coefficient file; create empty buffer
                    with open('IMU_cal_coeffs.txt', 'w') as f:
                        cal_list = []
                        #Iterate through unpacked coefficients, converting to strings of hex values
                        for i in cal_coeffs:
                            cal_list.append(str(hex(i)))
                        #Add all hex strings together, separated by commas. Write to file
                        cal_list = (','.join(cal_list))
                        f.write(cal_list)
                    #Close file, wait for a moment (for user safety) 
                    f.close()
                    print("IMU Calibration Complete!")
                    
                    
                    state = S2_READ
                            
                
                
            #State 2: read/update encoders to pass positional data to motortask
            elif state == S2_READ:
                myIMU.euler_angle()
                myIMU.angular_velo()
                
                rollShare.write(myIMU.roll)
                pitchShare.write(myIMU.pitch)
                
                rollvShare.write(myIMU.rollv)
                pitchvShare.write(myIMU.pitchv)
                
                
                
                
                pass
                
                
                
            
            yield state
            
        else:
            yield state